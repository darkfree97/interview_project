from django_elasticsearch_dsl import DocType, Index, fields
from test_app.models import Order, OrderItem, Product
from elasticsearch_dsl import analyzer

html_strip = analyzer(
    'html_strip',
    tokenizer="standard",
    filter=["standard", "lowercase", "stop", "snowball"],
    char_filter=["html_strip"]
)
orders = Index('orders')


@orders.doc_type
class OrderDocument(DocType):
    total_sum = fields.FloatField(attr="total_sum")
    items = fields.StringField(attr='items_indexing', multi=True, analyzer=html_strip)

    class Meta:
        model = Order
        fields = [
            'id',
            'date',
        ]

