from django.shortcuts import render

from search.documents import OrderDocument


def search(request):
    query = request.GET.get("query")
    if query:
        orders = OrderDocument.search().filter('regexp', items=".*{}.*".format(str(query).lower())).to_queryset()
    else:
        orders = None
        query = ''
    return render(request, 'search/search.html', {'orders': orders, 'query': query})


