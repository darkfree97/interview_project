from django.contrib import admin

from test_app.models import Order, OrderItem, Product, Category


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    fields = (
        "order_view",
        "items",
        "total_sum_view",
    )

    readonly_fields = (
        "order_view",
        "total_sum_view",
    )

    def order_view(self, obj):
        return str(obj)

    def total_sum_view(self, obj):
        return obj.total_sum

    order_view.short_description = "Description"
    total_sum_view.short_description = "Total"

admin.site.register(OrderItem)
admin.site.register(Product)
admin.site.register(Category)
