from django.urls import path
from . import views

app_name = 'test_app'

urlpatterns = [
    path('create/order/view/', views.create_order_view, name="index"),
    path('order/report/view/', views.get_report_view, name="order_report"),
    path('category/', views.CategoryList.as_view()),
    path('category/<int:pk>/', views.CategoryDetail.as_view()),
    path('category/<int:pk>/add/products/', views.add_product_to_category),
    path('category/<int:pk>/remove/products/', views.remove_product_to_category),
    path('product/', views.ProductList.as_view()),
    path('product/<int:pk>/', views.ProductDetail.as_view()),
    path('order/item/', views.OrderItemList.as_view()),
    path('order/item/<int:pk>/', views.OrderItemDetail.as_view()),
    path('order/', views.OrderList.as_view()),
    path('order/<int:pk>/', views.OrderDetail.as_view()),
    path('order/create/', views.new_order),
    path('order/report/', views.get_report),
]
