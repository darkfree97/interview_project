/**
 * Created by darkfree on 15.09.18.
 */
var app = angular.module("ReportApp", []);

app.controller("ReportCtrl", function ($scope, $http) {
    $scope.report_items = [];
    $http.get('../').then(request=>$scope.report_items = request.data);
});