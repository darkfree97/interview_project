var app = angular.module('OrderApp', []);

app.controller('CreateOrderCtrl', function ($scope, $http,) {
    $scope.products = [];
    $scope.selected_products = [];
    $scope.orders = [];

    $http.get("../../../product/").then(function (response) {
        $scope.products = response.data;
        console.log($scope.products);
    });

    $scope.AddProductToOrder = function (product) {
        $scope.selected_products.push({"product": product, "quantity": 0.0});
        for (var i = $scope.products.length - 1; i >= 0; i--) {
            if ($scope.products[i].id === product.id) {
                $scope.products.splice(i, 1);
            }
        }
    };

    $scope.RemoveProductFromOrder = function (product) {
        $scope.products.push(product);
        for (var i = $scope.selected_products.length - 1; i >= 0; i--) {
            if ($scope.selected_products[i].product.id === product.id) {
                $scope.selected_products.splice(i, 1);
            }
        }
    };

    $scope.order = function () {
        let request = [];
        $scope.selected_products.forEach(item => {
            request.push({"id": item.product.id, "quantity": item.quantity})
        });

        $http.post('../../../order/create/', {"order_items": request}).then(function (response) {
            $scope.orders.push(response.data);
            console.log(response.data);
        });
    }
});

app.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
}]);