from django.db import models
from django.utils import timezone


class Category(models.Model):
    class Meta:
        verbose_name_plural = "Categories"
        verbose_name = "category"

    name = models.CharField(verbose_name="Name", max_length=255)
    products = models.ManyToManyField(verbose_name="Products", to='Product', blank=True)

    def __str__(self):
        return self.name


class Product(models.Model):
    class Meta:
        verbose_name_plural = "Products"
        verbose_name = "product"

    name = models.CharField(verbose_name="Name of product", max_length=255)
    price = models.FloatField(verbose_name="Price", default=0.0)

    def __str__(self):
        return self.name


class Order(models.Model):
    class Meta:
        verbose_name_plural = "Orders"
        verbose_name = "order"

    items = models.ManyToManyField(verbose_name="Items", to='OrderItem')
    date = models.DateField(verbose_name="Order date", default=timezone.now)

    @property
    def total_sum(self):
        t_sum = 0
        for item in self.items.all():
            t_sum += item.order_item_price
        return t_sum

    @property
    def items_indexing(self):
        # Product for indexing. Used in Elasticsearch indexing.
        return [item.product.name for item in self.items.all()]

    def __str__(self):
        return "Order #{}".format(self.pk)


class OrderItem(models.Model):
    class Meta:
        verbose_name_plural = "Order items"
        verbose_name = "order item"

    product = models.ForeignKey(verbose_name="Product", to=Product, on_delete=models.CASCADE)
    quantity = models.FloatField(verbose_name="Quantity", default=0.0)

    @property
    def order_item_price(self):
        return self.product.price * self.quantity

    def __str__(self):
        return self.product.name

