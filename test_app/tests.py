from django.test import TestCase
from .serializers import *


class CategoryTestCase(TestCase):
    def setUp(self):
        s = CategorySerializer(data={
            "name": "test_category"
        })
        if s.is_valid():
            s.save()
            print(s.data)
        else:
            print("Data is invalid")
        self.created = s.data

    def test_is_category_created(self):
        self.assertNotEqual(self.created.get("id", 0), 0, "Category doesn't created")
        self.assertEqual(self.created.get("name", ""), "test_category")
        self.assertEqual(self.created.get("products", None), [])


class ProductTestCase(TestCase):
    def setUp(self):
        s = ProductSerializer(data={
            "name": "test_product",
            "price": 5.6
        })
        if s.is_valid():
            s.save()
            print(s.data)
        self.created = s.data

    def test_is_product_created(self):
        self.assertNotEqual(self.created.get("id", 0), 0, "Product doesn't created")
        self.assertEqual(self.created.get("name", ""), "test_product")
        self.assertEqual(self.created.get("price", 0.0), 5.6)

