from django.http import Http404
from django.shortcuts import render, get_object_or_404
from rest_framework import status
from rest_framework.decorators import api_view, parser_classes
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import *
from .models import *


class CategoryList(APIView):
    @staticmethod
    def get(request):
        return Response(CategorySerializer(Category.objects.all(), many=True).data)

    @staticmethod
    def post(request):
        serializer = CategorySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CategoryDetail(APIView):
    @staticmethod
    def get_object(pk):
        try:
            return Category.objects.get(pk=pk)
        except Category.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        return Response(CategorySerializer(self.get_object(pk)).data)

    def put(self, request, pk):
        serializer = CategorySerializer(self.get_object(pk), data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        self.get_object(pk).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ProductList(APIView):
    @staticmethod
    def get(request):
        return Response(ProductSerializer(Product.objects.all(), many=True).data)

    @staticmethod
    def post(request):
        serializer = ProductSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProductDetail(APIView):
    @staticmethod
    def get_object(pk):
        try:
            return Product.objects.get(pk=pk)
        except Product.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        return Response(ProductSerializer(self.get_object(pk)).data)

    def put(self, request, pk):
        serializer = ProductSerializer(self.get_object(pk), data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        self.get_object(pk).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class OrderItemList(APIView):
    @staticmethod
    def get(request):
        return Response(OrderItemSerializer(OrderItem.objects.all(), many=True).data)

    def post(self, request):
        serializer = OrderItemSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class OrderItemDetail(APIView):
    @staticmethod
    def get_object(pk):
        try:
            return OrderItem.objects.get(pk=pk)
        except OrderItem.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        return Response(OrderItemSerializer(self.get_object(pk)).data)

    def put(self, request, pk):
        serializer = OrderItemSerializer(self.get_object(pk), data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        self.get_object(pk).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class OrderList(APIView):
    @staticmethod
    def get(request):
        return Response(OrderSerializer(Order.objects.all(), many=True).data)


class OrderDetail(APIView):
    @staticmethod
    def get_object(pk):
        try:
            return Order.objects.get(pk=pk)
        except Order.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        return Response(OrderSerializer(self.get_object(pk)).data)

    def put(self, request, pk):
        serializer = OrderSerializer(self.get_object(pk), data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        self.get_object(pk).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['POST'])
def new_order(request):
    order = Order.objects.create()
    order_items = request.data.get("order_items", [])
    for item in order_items:
        if item.get("id", 0) is not 0 or item.get("product", "") is not "":
            order.items.add(
                OrderItem.objects.create(
                    product=Product.objects.create(name=item.get("product", ""),
                                                   price=item.get("price", 0.0)) if item.get("id",
                                                                                             0) is 0 else Product.objects.get(
                        pk=item.get("id", 0)),
                    quantity=item.get("quantity", 0.0)
                )
            )
    if order.items.count() is not 0:
        order.save()
        return Response(OrderSerializer(order).data, status=status.HTTP_201_CREATED)
    else:
        order.delete()
        return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST'])
def add_product_to_category(request, pk):
    if request.method == 'POST':
        product_ids_list = request.data.get("product_ids", [])
        if len(product_ids_list) is not 0 and isinstance(product_ids_list, list):
            category = get_object_or_404(Category, pk=pk)
            for p_id in product_ids_list:
                category.products.add(get_object_or_404(Product, pk=p_id))
            category.save()
            return Response(CategorySerializer(category).data)
        return Response(status=status.HTTP_400_BAD_REQUEST)
    else:
        return Response(CategorySerializer(get_object_or_404(Category, pk=pk)).data)


@api_view(['GET', 'POST'])
def remove_product_to_category(request, pk):
    if request.method == 'POST':
        product_ids_list = request.data.get("product_ids", [])
        if len(product_ids_list) is not 0 and isinstance(product_ids_list, list):
            category = get_object_or_404(Category, pk=pk)
            for p_id in product_ids_list:
                category.products.remove(get_object_or_404(Product, pk=p_id))
            category.save()
            return Response(CategorySerializer(category).data)
        return Response(status=status.HTTP_400_BAD_REQUEST)
    else:
        return Response(CategorySerializer(get_object_or_404(Category, pk=pk)).data)


@api_view(['GET'])
def get_report(request):
    response = {}
    for order in Order.objects.all():
        date = order.date.strftime("%Y-%m-%d")
        if response.get(date, None) is not None:
            response[date] += order.total_sum
        else:
            response[date] = order.total_sum
    return Response(response)


def create_order_view(request):
    return render(request, 'test_app/order_creation.html')


def get_report_view(request):
    return render(request, 'test_app/report.html')
