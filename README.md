Для запуску проекту для необхідно:
==================================
I. Запустити сервер elasticsearch
----------------------------------
На Ubuntu:
```
> cd elasticsearch-5.6.8/bin/
> ./elasticsearch
```
Необхідно мати встановлену Java VERSION<=8

На Windows:
```
> cd elasticsearch-5.6.8/bin/
> elasticsearch
```
Або запустити через менеджер
```
> elasticsearch-service manager
```
----------------------------------
Перевірити чи сервер працює за адресою:
```
localhost:9200
```
Без цього сервера django-elasticsearch-dsl не буде спровно працювати

II. Запустити сервер Django
----------------------------------
1) Створити virtualenv i імпортувати залежності:
```
> pipenv shell
> pip install -r requirements.txt
```
2)Виконати міграцію:
```
> python manage.py makemigrations
> python manage.py migrate
```
3) Створити суперюзера:
```
> python manage.py createsuperuser
```
4) Виконати індексацію elasticsearch:
```
> python manage.py search_index --rebuild
```
III. Перевірити роботу проекту.
-------------------------------
```
> python manage.py runserver
```
IV. API-urls
-------------------------------

category/
category/\<int:pk\>/

```
{
    "name":"Vegetables"
}
```
category/\<int:pk\>/add/products/
category/\<int:pk\>/remove/products/
```
{
    "product_ids":[
        1,
        2,
        ...
    ]
}
```
product/
product/\<int:pk\>/
```
{
    "name":"Salt",
    "price": 0.005
}
```
order/item/
order/item/\<int:pk\>/
```
{
    "quantity":12
}
```
order/
order/\<int:pk\>/
```
```
order/create/
```
{
    "order_items":[
        {
            "id":1,
            "quantity":20
        },
        {
            "id":2,
            "quantity":10.5
        }
    ]
}
```
order/report/
```
```

API використовується з використанням AngularJS


